# DLT Lab 3

Nikolai Mikriukov

- `Lab3.sol` - Solidity code to work with tokens
- `Lab3.html` - Frontend

This functions are defined in contract:

- sendToken
- buyToken
- checkToken
- makeToken
- editToken

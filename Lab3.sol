pragma solidity ^0.5.0;

contract ERC721{

    struct Token {
        string name;
        string img;
        bool exist;
    }

	mapping(address => Token)tokens;
    string names = "";
	string imgs = "";
	uint globalNum  =  1000;

    function sendToken(address addr, address _to) public {
        require (tokens[addr].exist);
        Token memory tkn = tokens[addr];
		tokens[_to] = tkn;

        delete tokens[addr];
		tokens[addr].exist = false;
	}

	function buyToken(address addr, address payable _from) public payable{
        require (tokens[_from].exist);
        require (msg.value == 100000);
        _from.transfer(msg.value);
        Token memory tkn = tokens[_from];
		tokens[addr] = tkn;

        delete tokens[_from];
		tokens[_from].exist = false;
	}

	function checkToken(address _owner) public view returns (string memory, string memory){
	    require (tokens[_owner].exist);
		return(tokens[_owner].name, tokens[_owner].img);
	}

	function makeToken(address addr, string memory title, string memory img) public payable{
	    require (msg.value == 100000);
	    Token memory tkn = Token(title, img, true);
        
        globalNum -= 1;
        tokens[addr] = tkn;
	}

	function editToken(address addr, string memory title, string memory img) public {
	    require (tokens[addr].exist);
	    Token memory tkn = Token(title, img, true);
	    tokens[addr] = tkn;
	}


}
